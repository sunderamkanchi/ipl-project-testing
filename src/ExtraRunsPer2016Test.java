import org.junit.Test;

import static org.testng.Assert.*;


public class ExtraRunsPer2016Test {

    @Test
    public void test(){
        ExtraRunsPer2016 obj = new ExtraRunsPer2016();
        String expectedData = "{Delhi Daredevils=106, Gujarat Lions=98, Kings XI Punjab=100, Kolkata Knight Riders=122, Mumbai Indians=102, Rising Pune Supergiants=108, Royal Challengers Bangalore=156, Sunrisers Hyderabad=107}";
        assertEquals(expectedData,obj.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.csv"));
    }

    @Test
    public void dataNoteRead(){
        ExtraRunsPer2016 pathData = new ExtraRunsPer2016();
        String expectedData =  "FileNotFoundException";
        assertEquals(expectedData,pathData.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.cs"));
    }

    @Test
    public void checkForNull(){
        ExtraRunsPer2016 nullData = new ExtraRunsPer2016();
        assertNotNull(nullData.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.csv"));
    }

    @Test
    public void CheckOutOfBound(){
        ExtraRunsPer2016 outOfBound = new ExtraRunsPer2016();
        String expectedData = "IndexOutOfBoundsException";
        assertTrue(expectedData == outOfBound.check("src/ExtraRunsPer2016.java","src/MatchWinnerCount.java") );
    }

    @Test
    public void ifNull(){
        ExtraRunsPer2016 checkNull = new ExtraRunsPer2016();
        assertNull(checkNull.check(null,null));

    }

}