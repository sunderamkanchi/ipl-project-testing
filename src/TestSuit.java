import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        MatchYearCountTest.class,
        MatchYearCountTest.class,
        ExtraRunsPer2016Test.class,
        TopEconomyBowlerTest.class,
        BowlingAnalysisTest.class
})

public class TestSuit {
    
}
