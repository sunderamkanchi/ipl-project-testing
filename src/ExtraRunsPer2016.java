import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class ExtraRunsPer2016 {
    public static void main(String[] args) {

        String matchPath = "src/MatchData/matches.csv";
        String deliveryPath = "src/DeliveryData/deliveries.csv";
        check(matchPath,deliveryPath);
        //System.out.println(check(matchPath,deliveryPath));
    }
    public static String check(String matchPath,String deliveryPath){

        String line = "";
        ArrayList matchId = new ArrayList();
        Map<String,Integer> extraRunCount = new TreeMap<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                if(values[1].equals("2016")){
                    matchId.add(values[0]);
                }
            }
            // System.out.println( matchId);


        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(deliveryPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                String deliveryId = values[0];
                String bowlingTeam = values[3];
                int extraRuns = Integer.parseInt(values[16]);


                if(matchId.contains(deliveryId)){
                    if(!(extraRunCount.containsKey(bowlingTeam))){
                        extraRunCount.put(bowlingTeam,0);
                    }

                    if(extraRunCount.containsKey(bowlingTeam)){
                        int count = extraRunCount.get(bowlingTeam)+extraRuns;
                        extraRunCount.put(bowlingTeam,count);
                    }
                }


            }
            //System.out.println(extraRunCount);

        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }
        return extraRunCount.toString();


    }
}
