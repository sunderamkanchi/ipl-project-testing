import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TopEconomyBowler {
    public static void main(String[] args) {
        String matchPath = "src/MatchData/matches.csv";
        String deliveryPath = "src/DeliveryData/deliveries.csv";
        check(matchPath,deliveryPath);
        System.out.println(check(matchPath,deliveryPath));

    }

    public static String check(String matchPath,String deliveryPath){


        String line = "";
        ArrayList matchId = new ArrayList<>();
        Map<String, TreeMap<String,Integer>> TopBowlers = new TreeMap<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(matchPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                if(values[1].equals("2015")){
                    matchId.add(values[0]);
                }

            }
            //System.out.println(matchId);

        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader(deliveryPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                String deliveryId = values[0];
                String bowler = values[8];
                int fullRunCount =Integer.parseInt(values[17]);


                if(matchId.contains(deliveryId)){
                    if(!(TopBowlers.containsKey(bowler))){
                        TopBowlers.put(bowler, new TreeMap<>());
                        TopBowlers.get(bowler).put("ballCount",0);
                        TopBowlers.get(bowler).put("totalRuns",0);
                        TopBowlers.get(bowler).put("economicalBowlers",0);
                    }

                    if(TopBowlers.containsKey(bowler)){
                        int ballTotal = TopBowlers.get(bowler).get("ballCount")+1;
                        int runsCount = TopBowlers.get(bowler).get("totalRuns")+fullRunCount;
                        TopBowlers.get(bowler).put("ballCount",ballTotal);
                        TopBowlers.get(bowler).put("totalRuns",runsCount);
                    }
                }
            }
            //System.out.println(TopBowlers);

        } catch (FileNotFoundException e) {
            //throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }
        Map<String,Double> topEconomicalBowler = new HashMap<>();

        TopBowlers.forEach((key,value) ->{
            int totalRunCount = value.get("totalRuns");
            int totalBallCount = value.get("ballCount");
            double over = totalBallCount/6;
            double economy = totalRunCount/over;
            String economyRate = String.format("%.3f",economy);
            //System.out.println(economyRate);
            topEconomicalBowler.put(key,Double.parseDouble(economyRate));

        });
        //System.out.println(topEconomicalBowler);



        Set<Map.Entry<String,Double>> bowler = topEconomicalBowler.entrySet();
        List<Map.Entry<String,Double>> topBowlerList = new ArrayList<>(bowler);

        Collections.sort(topBowlerList,(a,b) ->a.getValue().compareTo(b.getValue()));
        List topTenEconomicalBowlers = topBowlerList.subList(0,10);
        //System.out.println(topTenEconomicalBowlers);
        return topTenEconomicalBowlers.toString();

    }
}
