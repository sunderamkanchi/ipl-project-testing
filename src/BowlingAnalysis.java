import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class BowlingAnalysis {
    public static void main(String[] args) {

        String path = "src/DeliveryData/deliveries.csv";
        check(path);
//        System.out.println(check());
    }

    public static String check(String path){

        String line = "";
        Map<String, TreeMap<String, TreeMap<String,Integer>>> bowlingAnalysis = new TreeMap<>();

        try {
            BufferedReader bowlingData = new BufferedReader(new FileReader(path));
            bowlingData.readLine();
            while((line = bowlingData.readLine()) != null){
                String[] values = line.split(",");
//                System.out.println(Arrays.toString(values));
                String matchId = values[0];
                String bowlingTeam = values[3];
                String bowler = values[8];
                int fullRunCount =Integer.parseInt(values[17]);
                int wicketCount;


                if(matchId.equals("1")){

                    if (!(bowlingAnalysis.containsKey(bowlingTeam))){
                        bowlingAnalysis.put(bowlingTeam,new TreeMap<>());
                    }

                    if(!(bowlingAnalysis.get(bowlingTeam).containsKey(bowler))){

                        bowlingAnalysis.get(bowlingTeam).put(bowler, new TreeMap<>());
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("totalRuns",0);
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("wickets",0);
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("ballCount",0);
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("over",0);

                    }
                    if((bowlingAnalysis.get(bowlingTeam).containsKey(bowler))){
                        //bowlingAnalysis.get(bowlingTeam).put(bowler,new TreeMap<>());
                        int runCount = bowlingAnalysis.get(bowlingTeam).get(bowler).get("totalRuns")+fullRunCount;
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("totalRuns",runCount);
                        try{
                            String wicketTaken = values[18];
                            wicketCount = 1;
                        }
                        catch (Exception e) {
                            wicketCount = 0;
                        }

                        int countOfWicket = bowlingAnalysis.get(bowlingTeam).get(bowler).get("wickets") + wicketCount;
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("wickets",countOfWicket);

                        int ballCount = bowlingAnalysis.get(bowlingTeam).get(bowler).get("ballCount")+1;
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("ballCount",ballCount);

                        int overCount = ballCount/6;
                        bowlingAnalysis.get(bowlingTeam).get(bowler).put("over",overCount);

                    }
                }

            }
            //System.out.println(bowlingAnalysis);




        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }
        return bowlingAnalysis.toString();
    }
}
