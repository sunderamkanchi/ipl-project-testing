import org.junit.BeforeClass;
import org.junit.Test;

import static org.testng.Assert.*;

public class MatchYearCountTest {
    @Test
    public void test(){
        MatchYearCount sameOutput = new MatchYearCount();
        String expectedData =  "[2008=58, 2009=57, 2010=60, 2011=73, 2012=74, 2013=76, 2014=60, 2015=59, 2016=60, 2017=59]";
        assertEquals(expectedData,sameOutput.check("src/MatchData/matches.csv"));
    }

    @Test
    public  void dataNotRead(){
        MatchYearCount pathData = new MatchYearCount();
        String expectedData =  "FileNotFoundException";
        assertEquals(expectedData,pathData.check("src/MatchData/matches.cs"));
    }

    @Test
    public void CheckForNull(){
        MatchYearCount nullData = new MatchYearCount();
        assertNotNull(nullData.check("src/MatchData/matches.csv"));
    }

    @Test
    public  void checkOutOFBound(){
        MatchYearCount outOfBound = new MatchYearCount();
        String expectedData = "IndexOutOfBoundsException";
        assertTrue(expectedData == outOfBound.check("src/ExtraRunsPer2016.java") );

    }

    @Test
    public void ifNull(){
        MatchYearCount checkNull = new MatchYearCount();
        assertNull(checkNull.check(null));

    }

}