import org.junit.Test;

import static org.testng.Assert.*;


public class TopEconomyBowlerTest {


    @Test
    public void test(){
        TopEconomyBowler obj = new TopEconomyBowler();
        String expectedData = "[RN ten Doeschate=4.0, J Yadav=4.143, R Ashwin=5.725, S Nadeem=6.143, Parvez Rasool=6.2, MC Henriques=6.308, Z Khan=6.36, MA Starc=6.8, M Vijay=7.0, Sandeep Sharma=7.039]";
        assertEquals(expectedData,obj.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.csv"));
        //assertNotNull(obj.check());

    }

    @Test

    public void dataNotRead(){
        TopEconomyBowler pathData = new TopEconomyBowler();
        String expectedData =  "FileNotFoundException";
        assertEquals(expectedData,pathData.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.cs"));
    }

    @Test
    public void checkForNull(){
        TopEconomyBowler nullData = new TopEconomyBowler();
        assertNotNull(nullData.check("src/MatchData/matches.csv","src/DeliveryData/deliveries.csv"));
    }

    @Test
    public void checkOutOfBound(){
        TopEconomyBowler outOfBound = new TopEconomyBowler();
        String expectedData = "IndexOutOfBoundsException";
        assertTrue(expectedData == outOfBound.check("src/ExtraRunsPer2016.java","src/MatchWinnerCount.java") );
    }

    @Test
    public void ifNull(){
       TopEconomyBowler checkNull = new TopEconomyBowler();
        assertNull(checkNull.check(null,null));

    }

}