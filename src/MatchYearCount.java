import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MatchYearCount {


        public static void main(String[] args) {
            String path = "src/MatchData/matches.csv";
            System.out.println( check(path));
        }

        public static String check( String path){

            String line = "";
            Map<String,Integer> yearCount = new TreeMap<>();
            try {
                BufferedReader br = new BufferedReader(new FileReader(path));
                br.readLine();
                //System.out.println(br.readLine());

                while((line = br.readLine()) != null){
                    String[] values = line.split(",");

                    if((yearCount.containsKey(values[1])) == false){
                        yearCount.put(values[1],1);
                    }else{
                        int count = yearCount.get(values[1])+1;

                        yearCount.put(values[1],count);
                    }
                }
                //System.out.println(yearCount);
//                return(yearCount);


            } catch (FileNotFoundException e) {
//                throw new RuntimeException(e);
                return "FileNotFoundException";
            }catch (IOException e) {
                throw new RuntimeException(e);
            }catch (ArrayIndexOutOfBoundsException e){
                return "IndexOutOfBoundsException";
            }catch (NullPointerException e){
                return null;
            }

            Set<Map.Entry<String,Integer>> output = yearCount.entrySet();

           return output.toString();

        }
    }


