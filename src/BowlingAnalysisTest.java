import org.junit.Test;

import static org.testng.Assert.*;

public class BowlingAnalysisTest {

    @Test

    public void test(){
        BowlingAnalysis obj = new BowlingAnalysis();
        String exceptedData = "{Royal Challengers Bangalore={A Choudhary={ballCount=28, over=4, totalRuns=55, wickets=1}, S Aravind={ballCount=18, over=3, totalRuns=36, wickets=0}, SR Watson={ballCount=18, over=3, totalRuns=41, wickets=0}, STR Binny={ballCount=6, over=1, totalRuns=10, wickets=1}, TM Head={ballCount=6, over=1, totalRuns=11, wickets=0}, TS Mills={ballCount=25, over=4, totalRuns=32, wickets=1}, YS Chahal={ballCount=24, over=4, totalRuns=22, wickets=1}}, Sunrisers Hyderabad={A Nehra={ballCount=25, over=4, totalRuns=42, wickets=2}, B Kumar={ballCount=25, over=4, totalRuns=28, wickets=2}, BCJ Cutting={ballCount=24, over=4, totalRuns=35, wickets=1}, Bipul Sharma={ballCount=6, over=1, totalRuns=4, wickets=1}, DJ Hooda={ballCount=6, over=1, totalRuns=7, wickets=1}, MC Henriques={ballCount=13, over=2, totalRuns=20, wickets=1}, Rashid Khan={ballCount=24, over=4, totalRuns=36, wickets=2}}}";
        assertEquals(exceptedData,obj.check("src/DeliveryData/deliveries.csv"));
        assertNotNull(obj.check("src/DeliveryData/deliveries.csv"));
    }

    @Test
    public void dataNotRead(){
        BowlingAnalysis pathData = new BowlingAnalysis();
        String expectedData =  "FileNotFoundException";
        assertEquals(expectedData,pathData.check("src/DeliveryData/deliveries.cs"));

    }

    @Test
    public void checkForNull(){
        BowlingAnalysis nullData = new BowlingAnalysis();
        assertNotNull(nullData.check("src/DeliveryData/deliveries.cs"));
    }

    @Test
    public void checkOutOFBound(){
        BowlingAnalysis outOfBound = new BowlingAnalysis();
        String expectedData = "IndexOutOfBoundsException";
        assertTrue(expectedData == outOfBound.check("src/ExtraRunsPer2016.java") );
    }

    @Test
    public void ifNull(){
        BowlingAnalysis checkNull = new BowlingAnalysis();
        assertNull(checkNull.check(null));

    }


}