import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchWinnerCount {
    public static void main(String[] args) {
        String path = "src/MatchData/matches.csv";
        System.out.println(check(path));
       // checkResult();
    }

    public static String check(String path){

        String line = "";
        Map<String, TreeMap<String,Integer>> winnerCount = new TreeMap<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] values = line.split(",");

                if (values[10].equals("") ) {

                    values[10] = "Match tie";
                }

                if(winnerCount.containsKey(values[1]) == false){
                    winnerCount.put(values[1],new TreeMap<>());
                }
                if(winnerCount.containsKey(values[1])){
                    if(!(winnerCount.get(values[1]).containsKey(values[10]))){
                        winnerCount.get(values[1]).put(values[10],0);
                    }
                }
                if((winnerCount.get(values[1]).containsKey(values[10]))){
                    int count =winnerCount.get(values[1]).get(values[10])+1;
                    //System.out.println(count);
                    winnerCount.get(values[1]).put(values[10],count);


                }


            }
            //System.out.println(winnerCount);

        } catch (FileNotFoundException e) {
           // throw new RuntimeException(e);
            return "FileNotFoundException";
        }catch (IOException e) {
            throw new RuntimeException(e);
        }catch (ArrayIndexOutOfBoundsException e){
            return "IndexOutOfBoundsException";
        }catch (NullPointerException e){
            return null;
        }
        return winnerCount.toString();
    }
}

